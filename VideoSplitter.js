"use strict";

const { Storage } = require("@google-cloud/storage");
const { PubSub } = require("@google-cloud/pubsub");
const winston = require("winston");
const ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const path = require("path");
/**
 * Para la subida de iamgenes
 */
// The name of a storage class
// See the StorageClass documentation for other valid storage classes:
// https://googleapis.dev/java/google-cloud-clients/latest/com/google/cloud/storage/StorageClass.html
const storageClass = "standard";

// The name of a location
// See this documentation for other valid locations:
// http://g.co/cloud/storage/docs/locations#location-mr
const location = "US-CENTRAL1";

// Configuración de Winston para el sistema de logging
const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || "info",
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf(
      (info) => `${info.timestamp} - ${info.level}: ${info.message}`
    )
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: "./logs/video_splitter.log" }),
  ],
});
// Crear una instancia del cliente de Google Cloud Storage
const storageGcp = new Storage({
  projectId: "sdpp2-405215",
  // keyFilename: 'sdpp2-405215-a21bf250e80c.json',
  keyFilename: "/etc/secret-volume/sdpp2-405215-a21bf250e80c.json",
});
const bucketName = "videos-to-process-sobel";
const framesTopic = "projects/sdpp2-405215/topics/frames-to-process-topic";
const subscriptionNameOrId = "video-requests-topic-sub"; // Nombre de la suscripción
const pubSubClient = new PubSub({
  projectId: "sdpp2-405215",
  // keyFilename: 'sdpp2-405215-a21bf250e80c.json',
  keyFilename: "/etc/secret-volume/sdpp2-405215-a21bf250e80c.json",
});

//obtenemos la sub creada
const subscription = pubSubClient.subscription(subscriptionNameOrId);
// Create an event handler to handle messages
let messageCount = 0; //por si despues queremos sacar alguna estadistica
const messageHandler = async (message) => {
  try {
    logger.info(`[${message.id}] MSJ recibido del Video Receiver... `);
    // Convierte los datos en JSON
    const data = JSON.parse(message.data.toString("utf8"));

    // Accede al valor de "name" en el objeto JSON
    const videoPath = "/app/" + data.name;
    let CompleteNameVideo = data.name.split("-");
    let videoId = CompleteNameVideo[0];
    let totalFrames = -1;
    logger.info(`Procesando video: ${videoPath}`);

    //obtiene el video de gcs
    await getVideoFromGCS(videoPath, data.name);

    // Llama a la función para dividir el video en frames y guardarlos en un directorio
    totalFrames = await splitVideoIntoFrames(data.name, "frames");

    logger.info(`Se crearon ${totalFrames} frames.`);
    
    await createBucketWithStorageClassAndLocation(storageClass, videoId).catch(
      console.error
    );
    await uploadFilesToBucket(videoId, "frames", totalFrames);

    // Enviamos mensaje a Pub/Sub , informamos que hay frames a procesar
    const msj = JSON.stringify(
        { video: videoId,
          nameVideo:CompleteNameVideo[1],
        }
    );

    // Publicando mensaje para el delivery
    await publishMessage(msj);
    logger.info(`Mensaje de ${videoId} enviado para ser procesado por el Delivery.`);
    
    // Elimina la carpeta "frames" y el archivo "config.json" locales
    await cleanupFramesAndConfig();
  } catch (error) {
    logger.error("Error al procesar mensajes:", error);
  }
  // "Ack" (acknowledge receipt of) the message
  message.ack();
};
// Receive callbacks for errors on the subscription
subscription.on("error", (error) => {
  logger.error("Received error:", error);
  process.exit(1);
});

// Listen for new messages
subscription.on("message", messageHandler);

/////////////////////////////////////////----FUNCIONES-------------//////////////////////////////////////////////////////

// Obtener el video desde el bucket de GCS
async function getVideoFromGCS(videoPath, name) {
  logger.info("Solicitando el video a gcp storage");
  // getMetadata(videoPath).catch(console.error);

  async function downloadFile() {
    const options = {
      destination: videoPath,
    };

    // Downloads the file
    await storageGcp.bucket(bucketName).file(name).download(options);

    logger.info(`gs://${bucketName}/${name} downloaded`);
  }

  await downloadFile().catch(console.error);
}

async function uploadFilesToBucket(bucketName, directoryPath, totalFrames) {
  return new Promise(async (resolve, reject) => {
    logger.info("Subiendo frames al bucket...");

    try {
      const archivos = fs.readdirSync(directoryPath);
      const bucket = storageGcp.bucket(bucketName);

      const uploadPromises = archivos.map(async (archivo) => {
        await uploadToBucket(bucket, archivo);
      });

      // Espera a que todas las subidas se completen antes de continuar
      await Promise.all(uploadPromises);

      const configData = {
        total_frames: totalFrames,
      };

      const configJson = JSON.stringify(configData);

      // Guarda el objeto de configuración en un archivo llamado config.json
      fs.writeFileSync('config.json', configJson);

      // Sube el archivo de configuración al bucket
      await storageGcp.bucket(bucketName).upload('config.json', {
        destination: 'config.json',
      });

      logger.info("Archivo de configuración subido.");
      logger.info("Todos los frames y su configuración se han subido al bucket.");
      resolve();
    } catch (error) {
      logger.error("Error al subir los archivos.", error);
      reject(error);
    }
  });
}


async function createBucketWithStorageClassAndLocation(
  storageClass,
  bucketName
) {
  // For default values see: https://cloud.google.com/storage/docs/locations and
  // https://cloud.google.com/storage/docs/storage-classes
  logger.info("Creando bucket para subir frames...");
  const [bucket] = await storageGcp.createBucket(bucketName, {
    location,
    [storageClass]: true,
  });

  logger.info(
    `${bucket.name} created with ${storageClass} class in ${location}`
  );
}


// Función para eliminar la carpeta "frames" y el archivo "config.json"
async function cleanupFramesAndConfig() {
  try {
    // Elimina la carpeta "frames"
    await fs.promises.rm("frames", { recursive: true });

    // Elimina el archivo "config.json"
    await fs.promises.unlink("config.json");

    logger.info("Carpeta 'frames' y archivo 'config.json' eliminados.");
  } catch (error) {
    logger.error("Error al eliminar la carpeta 'frames' y el archivo 'config.json':", error);
  }
}


async function splitVideoIntoFrames(inputVideoPath, outputFramePath) {
  return new Promise((resolve, reject) => {
    // Crear la carpeta de frames si no existe
    if (!fs.existsSync(outputFramePath)) {
      fs.mkdirSync(outputFramePath);
    }

    logger.info(`Dividiendo Video en Frames...`);

    // Configurar la conversión de video a frames
    ffmpeg(inputVideoPath)
      .on("end", function () {
        // Obtener la lista de archivos en la carpeta de frames
        fs.readdir(outputFramePath, (err, files) => {
          if (err) {
            reject(err);
          } else {
            resolve(files.length);
          }
        });
      })
      .on("error", function (err) {
        reject(err);
      })
      .output(path.join(outputFramePath, "frame%04d.png"))
      .run();
  });
}

async function uploadToBucket(bucket,destFileName){
  const options = {
    destination: 'input/'+destFileName,
    // Optional:
    // Set a generation-match precondition to avoid potential race conditions
    // and data corruptions. The request to upload is aborted if the object's
    // generation number does not match your precondition. For a destination
    // object that does not yet exist, set the ifGenerationMatch precondition to 0
    // If the destination object already exists in your bucket, set instead a
    // generation-match precondition using its generation number.
    // preconditionOpts: {ifGenerationMatch: generationMatchPrecondition},
  };
    await bucket.upload('/app/frames/'+ destFileName, options);

}

// Mensaje de Inicializacion
function inicio() {
  logger.info("Iniciando Splitter.....");
}
inicio();

async function publishMessage(data) {
  // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
  const dataBuffer = Buffer.from(data);

  try {
    const messageId = await pubSubClient
      .topic(framesTopic)
      .publishMessage({data: dataBuffer});
    logger.info(`Mensaje ${messageId} publicado.`);
  } catch (error) {
    logger.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}


//////////////////////---------------------------------ya no se usa----------////////////////////////

async function getMetadata(videoPath) {
  // Gets the metadata for the file
  const [metadata] = await storageGcp
    .bucket(bucketName)
    .file(videoPath)
    .getMetadata();

  logger.info(`Bucket: ${metadata.bucket}`);
  logger.info(`CacheControl: ${metadata.cacheControl}`);
  logger.info(`ComponentCount: ${metadata.componentCount}`);
  logger.info(`ContentDisposition: ${metadata.contentDisposition}`);
  logger.info(`ContentEncoding: ${metadata.contentEncoding}`);
  logger.info(`ContentLanguage: ${metadata.contentLanguage}`);
  logger.info(`ContentType: ${metadata.contentType}`);
  logger.info(`CustomTime: ${metadata.customTime}`);
  logger.info(`Crc32c: ${metadata.crc32c}`);
  logger.info(`ETag: ${metadata.etag}`);
  logger.info(`Generation: ${metadata.generation}`);
  logger.info(`Id: ${metadata.id}`);
  logger.info(`KmsKeyName: ${metadata.kmsKeyName}`);
  logger.info(`Md5Hash: ${metadata.md5Hash}`);
  logger.info(`MediaLink: ${metadata.mediaLink}`);
  logger.info(`Metageneration: ${metadata.metageneration}`);
  logger.info(`Name: ${metadata.name}`);
  logger.info(`Size: ${metadata.size}`);
  logger.info(`StorageClass: ${metadata.storageClass}`);
  logger.info(`TimeCreated: ${new Date(metadata.timeCreated)}`);
  logger.info(`Last Metadata Update: ${new Date(metadata.updated)}`);
  logger.info(`TurboReplication: ${metadata.rpo}`);
  logger.info(
    `temporaryHold: ${metadata.temporaryHold ? "enabled" : "disabled"}`
  );
  logger.info(
    `eventBasedHold: ${metadata.eventBasedHold ? "enabled" : "disabled"}`
  );
  if (metadata.retentionExpirationTime) {
    logger.info(
      `retentionExpirationTime: ${new Date(metadata.retentionExpirationTime)}`
    );
  }
  if (metadata.metadata) {
    logger.info("\n\n\nUser metadata:");
    for (const key in metadata.metadata) {
      logger.info(`${key}=${metadata.metadata[key]}`);
    }
  }
}
