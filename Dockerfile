# Usa una imagen base de Node.js
FROM node:18

# Instala las dependencias necesarias para ffmpeg y ffprobe
RUN apt-get update && apt-get install -y ffmpeg && apt-get install -y ffmpeg

# Se crea un nuevo grupo de sistema(flag -r) llamado "user" 
RUN groupadd -r user

# Se crea un nuevo usuario de sistema(flag -r) y se agrega al grupo creado previamente (flag -g)
RUN useradd -r -g user user

# Establece el directorio de trabajo en el contenedor
WORKDIR /app

# Se cambian los npermisos del workdir al usuario no root "user"
RUN chown -R user:user /app

# Copia los archivos de tu aplicación al directorio de trabajo en el contenedor
COPY package*.json ./
COPY . .

# Instala las dependencias del proyecto
RUN npm install

# Se cambia al usuario no root "user"
USER user 

# Expone un puerto en el contenedor (por ejemplo, el puerto 3000 que podría usar una aplicación Node.js)
EXPOSE 8888

# Comando para ejecutar la aplicación cuando se inicie el contenedor
CMD [ "node", "VideoSplitter.js" ]
